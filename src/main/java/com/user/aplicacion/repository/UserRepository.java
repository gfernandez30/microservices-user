package com.user.aplicacion.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.user.aplicacion.model.User;

/**
* User Repository
*
* @author  Gabriel Fernandez
* @version 1.0
* @since   2018-10-26
*/
@Repository
public interface UserRepository extends CrudRepository<User, String> {

}
