package com.user.aplicacion.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.user.aplicacion.configuration.JwtUtil;
import com.user.aplicacion.model.Login;
import com.user.aplicacion.model.User;
import com.user.aplicacion.service.UserService;

/**
 * User Controller
 *
 * @author Gabriel Fernandez
 * @version 1.0
 * @since 2018-10-29
 */
@Controller
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private User user;

	@RequestMapping(value = "/user/list", method = RequestMethod.GET)
	public ResponseEntity<List<User>> getAllUsers() {
		return new ResponseEntity<List<User>>(userService.getAllUsers(), HttpStatus.OK);
	}

	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public ResponseEntity<User> getUserById(@PathVariable("id") String id) {
		return new ResponseEntity<User>(userService.getUserById(id), HttpStatus.OK);
	}

	@RequestMapping(value = "/user/save", method = RequestMethod.POST)
	public ResponseEntity<User> saveUser(@RequestBody User user) {
		return new ResponseEntity<User>(userService.saveUser(user), HttpStatus.OK);
	}

	@RequestMapping(value = "/user/update", method = RequestMethod.PUT)
	public ResponseEntity<User> updateUser(@RequestBody User campaign) {
		return new ResponseEntity<User>(userService.saveUser(campaign), HttpStatus.OK);
	}

	@RequestMapping(value = "/user/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> removeToDoById(@PathVariable("id") String id) {
		userService.deleteUser(id);
		return new ResponseEntity<String>("user delete", HttpStatus.OK);
	}

	@RequestMapping(value = "/user/login", method = RequestMethod.POST)
	public ResponseEntity<Object> login(@RequestBody final String jsonLogin) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		Login login = mapper.readValue(jsonLogin, Login.class);
		user = userService.getUserById(login.getUser());

		if (user != null) {
			if (userService.passwordDecode(user.getUsupass(), login.getPass())) {
				String token = JwtUtil.addAuthentication(user.getUsunomb());
				user.setUsupass("Bearer " + token);
				return new ResponseEntity<Object>(user, HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>(
						"{\n" + "	 \"code\": \"1\",\n" + "     \"message\": \"invalid password\"\n" + "}",
						HttpStatus.UNAUTHORIZED);
			}
		}
		return new ResponseEntity<Object>("{\n" + "	 \"code\": \"2\",\n" + "     \"message\": \"invalid user\"\n" + "}",
				HttpStatus.UNAUTHORIZED);
	}

}
