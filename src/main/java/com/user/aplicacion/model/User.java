package com.user.aplicacion.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * User Entity
 *
 * @author Gabriel Fernandez
 * @version 1.0
 * @since 2018-10-26
 */
@Entity
@Table(name = "MOD_USUARIO")
@Component
public class User {
	@Id
	@Column(name = "USUCODE")
	private String Usucode;
	
	@Column(name = "ROLCODE")
	private String Rolcode;
	
	@Column(name = "ACPCODE")
	private Integer Acpcode;
	
	@Column(name = "USUNOMB")
	private String Usunomb;
	
	@Column(name = "USUEMAIL")
	private String Usuemail;
	
	@Column(name = "USUAPROB")
	private String Usuaprob;
	
	@Column(name = "USUPASS")
	private String Usupass;	
	
	@Column(name = "TNDCODE")
	private Integer Tndcode;
	
	@Column(name = "ATRCODE")
	private String Atrcode;
		
	@Column(name = "USUJERVAL")
	private String Usujerval;
	
	@Column(name = "ROLCODEXT")
	private String Rolcodext;
	
	@Column(name = "USUESTADO")
	private String Usuestado;
	
	@Column(name = "USUAPE")
	private String Usuape;	
	
	@Column(name = "USUFECREA")
	private String Usufecrea;
	
	@Column(name = "USUFECULOG")
	private String Usufeculog;
	
	@Column(name = "USUFECHMOD")
	private Date Usufechmod;
	
	@Column(name = "USUTIPO")
	private String TipoUsuario;

	public String getUsucode() {
		return Usucode;
	}

	public void setUsucode(String usucode) {
		Usucode = usucode;
	}

	public String getRolcode() {
		return Rolcode;
	}

	public void setRolcode(String rolcode) {
		Rolcode = rolcode;
	}

	public Integer getAcpcode() {
		return Acpcode;
	}

	public void setAcpcode(Integer acpcode) {
		Acpcode = acpcode;
	}

	public String getUsunomb() {
		return Usunomb;
	}

	public void setUsunomb(String usunomb) {
		Usunomb = usunomb;
	}

	public String getUsuemail() {
		return Usuemail;
	}

	public void setUsuemail(String usuemail) {
		Usuemail = usuemail;
	}

	public String getUsuaprob() {
		return Usuaprob;
	}

	public void setUsuaprob(String usuaprob) {
		Usuaprob = usuaprob;
	}

	public String getUsupass() {
		return Usupass;
	}

	public void setUsupass(String usupass) {
		Usupass = usupass;
	}

	public Integer getTndcode() {
		return Tndcode;
	}

	public void setTndcode(Integer tndcode) {
		Tndcode = tndcode;
	}

	public String getAtrcode() {
		return Atrcode;
	}

	public void setAtrcode(String atrcode) {
		Atrcode = atrcode;
	}

	public String getUsujerval() {
		return Usujerval;
	}

	public void setUsujerval(String usujerval) {
		Usujerval = usujerval;
	}

	public String getRolcodext() {
		return Rolcodext;
	}

	public void setRolcodext(String rolcodext) {
		Rolcodext = rolcodext;
	}

	public String getUsuestado() {
		return Usuestado;
	}

	public void setUsuestado(String usuestado) {
		Usuestado = usuestado;
	}

	public String getUsuape() {
		return Usuape;
	}

	public void setUsuape(String usuape) {
		Usuape = usuape;
	}

	public String getUsufecrea() {
		return Usufecrea;
	}

	public void setUsufecrea(String usufecrea) {
		Usufecrea = usufecrea;
	}

	public String getUsufeculog() {
		return Usufeculog;
	}

	public void setUsufeculog(String usufeculog) {
		Usufeculog = usufeculog;
	}

	public Date getUsufechmod() {
		return Usufechmod;
	}

	public void setUsufechmod(Date usufechmod) {
		Usufechmod = usufechmod;
	}

	public String getTipoUsuario() {
		return TipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		TipoUsuario = tipoUsuario;
	}
	
}
