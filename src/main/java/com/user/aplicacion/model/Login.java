package com.user.aplicacion.model;

/**
 * Login
 *
 * @author Gabriel Fernandez
 * @version 1.0
 * @since 2018-10-29
 */
public class Login {
	
	private String User;
	private String Pass;
	
	public String getUser() {
		return User;
	}
	public void setUser(String user) {
		User = user;
	}
	public String getPass() {
		return Pass;
	}
	public void setPass(String pass) {
		Pass = pass;
	}
	
	@Override
	public String toString() {
		return "Login [User=" + User + ", Pass=" + Pass + "]";
	}	

}
