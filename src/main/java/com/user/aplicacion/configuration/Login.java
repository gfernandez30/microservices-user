package com.user.aplicacion.configuration;

/**
 * Login
 *
 * @author Gabriel Fernandez
 * @version 1.0
 * @since 2018-10-29
 */
public class Login {
	
	private String user;
	private String pass;
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	

}
