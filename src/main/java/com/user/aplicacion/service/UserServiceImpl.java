package com.user.aplicacion.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.user.aplicacion.model.User;
import com.user.aplicacion.repository.UserRepository;

/**
 * User ServiceImpl
 *
 * @author Gabriel Fernandez
 * @version 1.0
 * @since 2018-10-26
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PasswordEncoder bCryptPasswordEncoder;

	@Override
	public User saveUser(User user) {
		user.setUsupass(bCryptPasswordEncoder.encode(user.getUsupass()));
		return userRepository.save(user);
	}

	@Override
	public List<User> getAllUsers() {
		List<User> list = new ArrayList<>();
		userRepository.findAll().forEach(e -> list.add(e));
		return list;
	}

	@Override
	public void deleteUser(String userId) {
		userRepository.deleteById(userId);
	}

	@Override
	public User getUserById(String userId) {
		Optional<User> user = userRepository.findById(userId);
		if (user.isPresent()) {
			User u = user.get();
			return u;
		}
		return null;
	}

	@Override
	public boolean passwordDecode(String passEncode, String pass) {		
		return bCryptPasswordEncoder.matches(pass, passEncode);
	}

}
