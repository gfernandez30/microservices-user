package com.user.aplicacion.service;

import java.util.List;

import com.user.aplicacion.model.User;

// TODO: Auto-generated Javadoc
/**
 * User Service.
 *
 * @author Gabriel Fernandez
 * @version 1.0
 * @since 2018-10-26
 */
public interface UserService {
	
	/**
	 * Save promotion.
	 *
	 * @param user the user
	 * @return the user
	 */
	public User saveUser(User user);

	/**
	 * Gets the all users.
	 *
	 * @return the all users
	 */
	public List<User> getAllUsers();

	/**
	 * Delete user.
	 *
	 * @param userId the user id
	 */
	public void deleteUser(String userId);

	/**
	 * Gets the user by id.
	 *
	 * @param userId the user id
	 * @return the user by id
	 */
	public User getUserById(String userId);
	
	/**
	 * Password decode.
	 *
	 * @param passEncoder the pass encoder
	 * @param pass the pass
	 * @return true, if successful
	 */
	public boolean passwordDecode(String passEncode, String pass);

}
