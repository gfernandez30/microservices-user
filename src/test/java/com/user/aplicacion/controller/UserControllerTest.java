package com.user.aplicacion.controller;

import com.user.aplicacion.service.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.context.ActiveProfiles;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserControllerTest {

    MockMvc mockMvc;

    @InjectMocks
    UserController qrController;

    @Mock
    UserServiceImpl userServiceImpl;

    String user_id = "admin";

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(qrController).build();
    }

    @Test
    public void getAllUsers() throws Exception {
        this.mockMvc.perform(get("/user/list"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getUserById() throws Exception {
        this.mockMvc.perform(get("/user/" +  user_id))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
