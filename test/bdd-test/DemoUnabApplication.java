package com.example.demounab;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.lang.*;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoUnabApplication implements CommandLineRunner {

    ExtentHtmlReporter htmlReporter;
    ExtentReports extent;
    ExtentTest logger;


    public static void main(String[] args) {
        SpringApplication.run(DemoUnabApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        try {

            htmlReporter = new ExtentHtmlReporter("C:\\Users\\gabri\\OneDrive\\Documentos\\unab\\74\\reporte-unab.html");
            extent = new ExtentReports();
            extent.attachReporter(htmlReporter);

            ExtentTest logger = extent.createTest("Login Portal Unab");
            logger.log(Status.PASS, "Inicio");

            WebDriver driver;
//https://www.youtube.com/watch?v=mh9QSLoX0nM
            System.setProperty("webdriver.chrome.driver",
                    "C:\\Users\\gabri\\OneDrive\\Documentos\\unab\\74\\chromedriver.exe");

            driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.get("https://portal.unab.cl/");
            logger.log(Status.PASS, "Ingreso a la URL https://portal.unab.cl/");

            driver.findElement(By.id("Usuario")).sendKeys("g.fernandezvelasquez");
            logger.log(Status.PASS, "Ingreso ususrio g.fernandezvelasquez");
            pausa(500);
            driver.findElement(By.id("Clave")).sendKeys("XXXXXXX");
            logger.log(Status.PASS, "Ingreso clave *********");
            pausa(500);
            driver.findElement(By.id("btnEnviar")).click();
            logger.log(Status.PASS, "click en el boton ingresar");
            pausa(500);
            driver.findElement(By.cssSelector("li:nth-child(1) .info-horario > div:nth-child(2)")).click();
            logger.log(Status.PASS, "click en el horario de Unab del semestre en curso");
            pausa(500);
            logger.log(Status.PASS, "Fin");
            extent.flush();
        } catch (Exception e) {
            logger.log(Status.FAIL, "Error " + e);
        }
    }

    public static void pausa(long sleeptime) {
        try {
            Thread.sleep(sleeptime);
        } catch (InterruptedException ex) {
        }
    }

}
